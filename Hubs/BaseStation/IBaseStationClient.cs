﻿using System.Threading.Tasks;

namespace BiovegAPI.Hubs.BaseStation
{
    public interface IBaseStationClient
    {
        Task ReceiveMessage(string response);
    }
}   