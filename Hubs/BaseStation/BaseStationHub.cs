﻿using System.Threading.Tasks;
using BiovegAPI.Models.Sensor;
using BiovegAPI.Repositories;
using BiovegAPI.Repositories.Sensor;
using BiovegAPI.Services.Logger;
using Microsoft.AspNetCore.SignalR;

namespace BiovegAPI.Hubs.BaseStation
{
    public class BaseStationHub : Hub<IBaseStationClient>
    {
        private readonly IInfluxRepository<SensorData> influxRepository;
        private readonly ILogger logger;

        public BaseStationHub(IInfluxRepository<SensorData> influxRepository, ILogger logger)
        {
            this.influxRepository = influxRepository;
            this.logger = logger;
        }

        public Task Dispatch(SensorData sensorData)
        {
            this.influxRepository.AddAsync(sensorData);
            return Clients.Caller.ReceiveMessage(
                $"Added sensor data:\n" +
                $"ID: {sensorData.Id}\n" +
                $"Type: {sensorData.Type}\n" +
                $"Value: {sensorData.Value}");
        }
    }
}