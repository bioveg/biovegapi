﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.PlantCondition;
using BiovegAPI.Models.Sensor;
using BiovegAPI.Models.ValueTime;
using BiovegAPI.Repositories;
using Microsoft.AspNetCore.SignalR;

namespace BiovegAPI.Hubs.WebService
{
    public class WebServiceHub : Hub<IWebServiceClient>
    {
        private readonly IInfluxRepository<SensorData> sensorInfluxRepo;

        public WebServiceHub(IInfluxRepository<SensorData> sensorInfluxRepo)
            => this.sensorInfluxRepo = sensorInfluxRepo;

        public async Task GetPlantConditions(string plantName)
        {
        }

        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }

        public async Task GetSensorData(string sensorId, int type, int seconds)
        {
            List<ValueTime> valueTimes = await this.sensorInfluxRepo.GetPeriodAsync(seconds);
            await Clients.Caller.ReceiveSensorData(valueTimes);

        }
    }
}