﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.ValueTime;

namespace BiovegAPI.Hubs.WebService
{
    public interface IWebServiceClient
    {
        Task ReceiveSensorData(List<ValueTime> valueTimes);
        Task ReceiveSensorData(ValueTime valueTime);
    }
}