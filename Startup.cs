using System;
using BiovegAPI.Hubs.BaseStation;
using BiovegAPI.Hubs.WebService;
using BiovegAPI.Models.Sensor;
using BiovegAPI.Repositories;
using BiovegAPI.Repositories.Greenhouse;
using BiovegAPI.Repositories.Plant;
using BiovegAPI.Repositories.PlantCondition;
using BiovegAPI.Repositories.Sensor;
using BiovegAPI.Services.Influx;
using BiovegAPI.Services.Logger;
using BiovegAPI.Services.Postgres;
using InfluxDB.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Npgsql;

namespace BiovegAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors(o =>
                o.AddPolicy("CorsPolicy2", p =>
                    p.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()));
          // services.AddCors(o =>
          //     o.AddPolicy("CorsPolicy", p =>
          //         p.AllowAnyHeader().AllowAnyMethod().SetIsOriginAllowed(y => _ = true).AllowCredentials()));
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo {Title = "BiovegAPI", Version = "v1"}); });
            services.AddSignalR(options => { options.EnableDetailedErrors = true; });
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto |
                                           ForwardedHeaders.XForwardedHost;
            });

            var sensorInfluxDbOptions = InfluxDBClientOptions.Builder.CreateNew()
                .AuthenticateToken(Settings.InfluxDbToken)
                .Bucket(Settings.InfluxDbBucketSensor)
                .Org(Settings.InfluxDbOrgBioveg)
                .Url(Settings.InfluxDbUrl)
                .Build();
            var npgSqlConn = new NpgsqlConnection(Settings.PgConnString);

            // Register services
            services.AddScoped<IInfluxRepository<SensorData>>(serviceProvider =>
                new SensorInfluxRepository(new InfluxService(
                    InfluxDBClientFactory.Create(sensorInfluxDbOptions),
                    sensorInfluxDbOptions,
                    serviceProvider.GetService<ILogger>()), serviceProvider.GetService<ISensorPostgresRepository>()));
            services.AddSingleton(npgSqlConn);
            services.AddScoped<ISensorPostgresRepository, SensorPostgresRepository>();
            services.AddScoped<ILogger, Logger>();
            services.AddScoped<IInfluxService, InfluxService>();
            services.AddScoped<IPostgresService, PostgresService>();
            services.AddScoped<IPlantConditionRepository, PlantConditionRepository>();
            services.AddScoped<IGreenhouseRepository, GreenhouseRepository>();
            services.AddScoped<IPlantRepository, PlantRepository>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("CorsPolicy2");
            
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BiovegAPI"));
            //app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseForwardedHeaders();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<BaseStationHub>("/BaseStationHub");
                endpoints.MapHub<WebServiceHub>("/WebServiceHub");
                endpoints.MapControllers();
            });
        }
    }
}