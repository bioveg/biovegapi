using System;
using System.Net;
using Microsoft.AspNetCore.Hosting;

namespace BiovegAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Bioveg API V0.1");
            var host = new WebHostBuilder()
                .UseKestrel(options =>
                {
                    options.AddServerHeader = false;
                    options.Listen(IPAddress.Any, 5000);
                })
                .UseStartup<Startup>().Build();
            host.Run();
        }
    }
}