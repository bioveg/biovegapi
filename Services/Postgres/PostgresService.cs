﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using BiovegAPI.Services.Logger;
using Npgsql;

namespace BiovegAPI.Services.Postgres
{
    public class PostgresService : IPostgresService
    {
        private readonly NpgsqlConnection conn;
        private readonly ILogger logger;

        public PostgresService(NpgsqlConnection conn, ILogger logger)
        {
            this.conn = conn;
            this.logger = logger;
        }

        public async Task PostAsync(NpgsqlCommand command)
        {
            try
            {
                ConsoleOut(command.CommandText);
                await this.conn.OpenAsync();
                command.Connection = this.conn;
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                await this.conn.CloseAsync();
            }
        }

        public async Task<List<string[]>> GetAllAsync<T>(NpgsqlCommand command)
        {
            try
            {
                ConsoleOut(command.CommandText);
                while (conn.State != ConnectionState.Closed)
                {
                    await Task.Delay(50);
                }
                await this.conn.OpenAsync();
                command.Connection = this.conn;
                var reader = await command.ExecuteReaderAsync();
                // Quick fix to get it to work, better solution would be using UDT
                List<string[]> returnList = new();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string[] stringArray = new string[reader.FieldCount];
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            stringArray[i] = reader.GetValue(i).ToString();
                        }
                    
                        returnList.Add(stringArray);
                    }    
                }
                return returnList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                await this.conn.CloseAsync();
            }
        }

        private void ConsoleOut(string command) => this.logger.ConsoleOut($"[Postgres] Executing command: {command}");
    }
}