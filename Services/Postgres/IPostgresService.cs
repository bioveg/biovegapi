﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Npgsql;

namespace BiovegAPI.Services.Postgres
{
    public interface IPostgresService
    {
        Task PostAsync(NpgsqlCommand command);
        Task<List<string[]>> GetAllAsync<T>(NpgsqlCommand command);
    }
}