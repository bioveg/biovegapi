﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.ValueTime;
using BiovegAPI.Services.Logger;
using CsvHelper;
using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Core.Flux.Domain;

namespace BiovegAPI.Services.Influx
{
    public class InfluxService : IInfluxService
    {
        private readonly InfluxDBClient client;
        private readonly InfluxDBClientOptions options;
        private readonly ILogger logger;

        public InfluxService(InfluxDBClient client, InfluxDBClientOptions options, ILogger logger)
        {
            this.client = client;
            this.options = options;
            this.logger = logger;
        }

        public async Task PostMeasurementAsync(string data)
        {
            var writeApi = this.client.GetWriteApiAsync();
            await writeApi.WriteRecordAsync(this.options.Bucket, this.options.Org, WritePrecision.Ns, data);
        }

        public async Task<T> GetCurrentMeasurementAsync<T>()
        {
            List<FluxTable> fluxTables = await GetTablesAsync(0);
            List<FluxRecord> records = fluxTables[0].Records;
            var data = (T) records[0].GetValue();
            return await Task.FromResult(data);
        }

        public async Task<List<FluxRecord>> GetMeasurementsAsync<T>(int fromSecondsAgo)
        {
            List<FluxTable> fluxTables = await GetTablesAsync(fromSecondsAgo);
            List<FluxRecord> records = fluxTables[0].Records;
            return records;
        }

        private async Task<List<FluxTable>> GetTablesAsync(int secondsAgo)
        {
            string flux = $"from(bucket:\"{this.options.Bucket}\") |> range(start: -{secondsAgo}s)";
            Console.WriteLine(flux);
            return await this.client.GetQueryApi().QueryAsync(flux);
        }

        private void ConsoleOut(string message) => this.logger.ConsoleOut(message);
    }
}