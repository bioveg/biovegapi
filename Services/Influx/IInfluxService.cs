﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfluxDB.Client.Core.Flux.Domain;

namespace BiovegAPI.Services.Influx
{
    public interface IInfluxService
    {
        Task<List<FluxRecord>> GetMeasurementsAsync<T>(int fromSecondsAgo);
        Task<T> GetCurrentMeasurementAsync<T>();
        Task PostMeasurementAsync(string data);
    }
}