﻿namespace BiovegAPI.Services.Logger
{
    public interface ILogger
    {
        void ConsoleOut(string message);
        void Log(string message);
    }
}