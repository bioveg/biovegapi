﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.Sensor;

namespace BiovegAPI.Repositories.Sensor
{
    public interface ISensorPostgresRepository
    {
        Task<List<Models.Sensor.Sensor>> GetSensors(int greenhouseId);
        Task PostAsync(string id, int greenhouseId, string plant);
    }
}