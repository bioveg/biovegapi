﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using BiovegAPI.Models.Greenhouse;
using BiovegAPI.Models.Sensor;
using BiovegAPI.Services.Postgres;
using Npgsql;
using NpgsqlTypes;

namespace BiovegAPI.Repositories.Sensor
{
    public class SensorPostgresRepository : ISensorPostgresRepository
    {
        private readonly IPostgresService postgresService;

        public SensorPostgresRepository(IPostgresService postgresService) =>
            this.postgresService = postgresService;

        public async Task<List<Models.Sensor.Sensor>> GetSensors(int greenhouseId)
        {
            var command = new NpgsqlCommand
            {
                Parameters =
                {
                    new NpgsqlParameter("greenhouseid", greenhouseId)
                    {
                        NpgsqlDbType = NpgsqlDbType.Integer, Direction = ParameterDirection.Input
                    },
                    new NpgsqlParameter("plant", NpgsqlDbType.Text)
                    {
                        Direction = ParameterDirection.Output
                    },
                    new NpgsqlParameter("id", NpgsqlDbType.Text)
                    {
                        Direction = ParameterDirection.Output
                    }
                },
                CommandText = "get_sensors",
                CommandType = CommandType.StoredProcedure
            };

            // Quick fix to get it to work, better solution would be using UDT (UserDefined Type)
            var dataList = await this.postgresService.GetAllAsync<Models.Sensor.Sensor>(command);
            var returnList = new List<Models.Sensor.Sensor>();
            if (dataList.Count > 0)
            {
                foreach (var stringArray in dataList)
                {
                    returnList.Add(new Models.Sensor.Sensor(stringArray[0], stringArray[1]));
                }    
            }
            return returnList;
        }

        public async Task PostAsync(string id, int greenhouseId, string plant)
        {
            var command = new NpgsqlCommand
            {
                Parameters =
                {
                    new NpgsqlParameter("iid", id)
                    {
                        NpgsqlDbType = NpgsqlDbType.Text, Direction = ParameterDirection.Input
                    },
                    new NpgsqlParameter("iplant_name", plant)
                    {
                        NpgsqlDbType = NpgsqlDbType.Text, Direction = ParameterDirection.Input
                    },
                    new NpgsqlParameter("igreenhouse_id", greenhouseId)
                    {
                        NpgsqlDbType = NpgsqlDbType.Integer, Direction = ParameterDirection.Input
                    }
                },
                CommandText = "create_sensor",
                CommandType = CommandType.StoredProcedure,
            };

            // Quick fix to get it to work, better solution would be using UDT
            await this.postgresService.PostAsync(command);
        }
    }
}