﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.Sensor;
using BiovegAPI.Models.ValueTime;
using BiovegAPI.Services.Influx;
using InfluxDB.Client.Core.Flux.Domain;
using NodaTime;

namespace BiovegAPI.Repositories.Sensor
{
    public class SensorInfluxRepository : IInfluxRepository<SensorData>
    {
        private readonly IInfluxService influxService;
        private readonly ISensorPostgresRepository pgSensorRepository;

        public SensorInfluxRepository(IInfluxService influxService, ISensorPostgresRepository pgSensorRepository)
        {
            this.influxService = influxService;
            this.pgSensorRepository = pgSensorRepository;
        }

        public async Task AddAsync(SensorData sensorData)
        {
            var type = sensorData.GetSensorType();
            string data = $"temp,sensor={sensorData.Id} {type.ToString()}={sensorData.Value}";
            await this.influxService.PostMeasurementAsync(data);
        }

        public async Task<List<ValueTime>> GetPeriodAsync(int seconds)
        {
            List<ValueTime> returnList = new List<ValueTime>();
            try
            {
                List<FluxRecord> data = await this.influxService.GetMeasurementsAsync<ValueTime>(seconds);
                foreach (FluxRecord fluxRecord in data)
                {
                    double f = (double)fluxRecord.GetValue();
                    var s = fluxRecord.GetTime();
                    if (s.HasValue)
                    {
                        returnList.Add(new ValueTime(float.Parse(f.ToString()),s.Value.InUtc().ToDateTimeUtc() ));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return returnList;
        }

        public async Task<ValueTime> GetCurrentAsync()
        {
            return await this.influxService.GetCurrentMeasurementAsync<ValueTime>();
        }
    }
}