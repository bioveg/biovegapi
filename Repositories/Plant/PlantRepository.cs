﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using BiovegAPI.Models.Greenhouse;
using BiovegAPI.Models.Plant;
using BiovegAPI.Models.PlantCondition;
using BiovegAPI.Services.Logger;
using BiovegAPI.Services.Postgres;
using Npgsql;
using NpgsqlTypes;

namespace BiovegAPI.Repositories.Plant
{
    public class PlantRepository : IPlantRepository
    {
        private readonly IPostgresService postgresService;
        private readonly ILogger logger;

        public PlantRepository(IPostgresService postgresService, ILogger logger)
        {
            this.postgresService = postgresService;
            this.logger = logger;
        }

        public async Task<List<PlantData>> GetAllAsync()
        {
            var command = new NpgsqlCommand
            {
                Parameters =
                {
                    
                },
                CommandText = "get_plants",
                CommandType = CommandType.StoredProcedure
            };
            
            // Quick fix to get it to work, better solution would be using UDT (UserDefined Type)
            var dataList = await this.postgresService.GetAllAsync<PlantData>(command);
            var returnList = new List<PlantData>();
            foreach (var stringArray in dataList)
            {
                returnList.Add(new PlantData(stringArray[0]));
            }

            return returnList;
        }
    }
}