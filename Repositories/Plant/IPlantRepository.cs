﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.Plant;

namespace BiovegAPI.Repositories.Plant
{
    public interface IPlantRepository
    {
        Task<List<PlantData>> GetAllAsync();
    }
}