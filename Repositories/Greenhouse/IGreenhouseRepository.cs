﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.Greenhouse;

namespace BiovegAPI.Repositories.Greenhouse
{
    public interface IGreenhouseRepository
    {
        public Task<List<GreenhouseData>> GetAllAsync(string email);
        public Task PostAsync(GreenHouseDataWithEmail greenhouseData);
    }
}