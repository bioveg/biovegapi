﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using BiovegAPI.Models.Greenhouse;
using BiovegAPI.Services.Postgres;
using Npgsql;
using NpgsqlTypes;

namespace BiovegAPI.Repositories.Greenhouse
{
    public class GreenhouseRepository : IGreenhouseRepository
    {
        private readonly IPostgresService postgresService;

        public GreenhouseRepository(IPostgresService postgresService) =>
            this.postgresService = postgresService;

        public async Task<List<GreenhouseData>> GetAllAsync(string email)
        {
            var command = new NpgsqlCommand
            {
                Parameters =
                {
                    new NpgsqlParameter("email", email)
                    {
                        NpgsqlDbType = NpgsqlDbType.Text, Direction = ParameterDirection.Input
                    },
                    new NpgsqlParameter("id", NpgsqlDbType.Integer)
                    {
                        Direction = ParameterDirection.Output
                    },
                    new NpgsqlParameter("name", NpgsqlDbType.Text)
                    {
                        Direction = ParameterDirection.Output
                    }
                },
                CommandText = "get_greenhouses",
                CommandType = CommandType.StoredProcedure
            };
            
            // Quick fix to get it to work, better solution would be using UDT (UserDefined Type)
            var dataList = await this.postgresService.GetAllAsync<GreenhouseData>(command);
            var returnList = new List<GreenhouseData>();
            foreach (var stringArray in dataList)
            {
                returnList.Add(new GreenhouseData(int.Parse(stringArray[0]), stringArray[1]));
            }

            return returnList;
        }
        
        public async Task PostAsync(GreenHouseDataWithEmail greenhouseData)
        {
            var command = new NpgsqlCommand
            {
                Parameters =
                {
                    new NpgsqlParameter("iname", greenhouseData.Name)
                    {
                        NpgsqlDbType = NpgsqlDbType.Text, Direction = ParameterDirection.Input
                    },
                    new NpgsqlParameter("iuser_email", greenhouseData.Email)
                    {
                        NpgsqlDbType = NpgsqlDbType.Text, Direction = ParameterDirection.Input
                    }
                },
                CommandText = "create_greenhouse",
                CommandType = CommandType.StoredProcedure,
                
            };

            // Quick fix to get it to work, better solution would be using UDT
            await this.postgresService.PostAsync(command);


            //var command = new NpgsqlCommand
            //{
            //    Parameters =
            //    {
            //        new NpgsqlParameter("id", greenhouseData.Id)
            //        {
            //            NpgsqlDbType = NpgsqlDbType.Integer, Direction = ParameterDirection.Input
            //        },
            //        new NpgsqlParameter("name", greenhouseData.Name)
            //        {
            //            NpgsqlDbType = NpgsqlDbType.Text, Direction = ParameterDirection.Input
            //        }
            //    },
            //    CommandText = "create_greenhouse",
            //    CommandType = CommandType.StoredProcedure
            //};

            //await this.postgresService.PostAsync(command);
        }
    }
}