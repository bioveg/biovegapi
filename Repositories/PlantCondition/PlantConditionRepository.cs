﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.PlantCondition;
using BiovegAPI.Services.Logger;
using BiovegAPI.Services.Postgres;

namespace BiovegAPI.Repositories.PlantCondition
{
    public class PlantConditionRepository : IPlantConditionRepository
    {
        private readonly IPostgresService postgresService;
        private readonly ILogger logger;

        public PlantConditionRepository(IPostgresService postgresService, ILogger logger)
        {
            this.postgresService = postgresService;
            this.logger = logger;
        }

        public async Task<List<PlantConditionData>> GetAllAsync(string plantName)
        {
            // this.logger.ConsoleOut($"Getting PlantConditionData from {plantName}");
            // Task<List<PlantConditionData>> task =
            //     this.postgresService.GetAllAsync<PlantConditionData>("ups_read_plant_conditions", plantName);
            // this.logger.ConsoleOut($"Returned: Object[{nameof(task.Result)}] Amount[{task.Result.Count}]");
            // return await task;
            return null;
        }
    }
}