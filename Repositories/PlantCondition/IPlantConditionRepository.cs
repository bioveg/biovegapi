﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.PlantCondition;

namespace BiovegAPI.Repositories.PlantCondition
{
    public interface IPlantConditionRepository
    {
        Task<List<PlantConditionData>> GetAllAsync(string plantName);
    }
}