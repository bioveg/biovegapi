﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.ValueTime;

namespace BiovegAPI.Repositories
{
    public interface IInfluxRepository<T>
    {
        Task AddAsync(T data);
        Task<List<ValueTime>> GetPeriodAsync(int seconds);
        Task<ValueTime> GetCurrentAsync();
    }
}