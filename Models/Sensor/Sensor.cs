namespace BiovegAPI.Models.Sensor
{
    public class Sensor
    {
        public string Plant { get; set; }
        public string Id { get; set; }

        public Sensor(string plant, string id)
        {
            this.Plant = plant;
            this.Id = id;
        }
    }
}