﻿namespace BiovegAPI.Models.Sensor
{
    public enum SensorType
    {
        AirHumidity,
        AirTemperature,
        SoilHumidity,
        SoilTemperature
    }
}