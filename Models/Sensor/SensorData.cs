﻿using System;
using InfluxDB.Client.Core;

namespace BiovegAPI.Models.Sensor
{
    [Measurement("sensor")]
    public class SensorData
    {
        public SensorData(string id, int type, float value)
        {
            this.Id = id;
            this.Type = type;
            this.Value = value;
        }

        [Column("id", IsTag = true)] public string Id { get; set; }
        [Column("type")] public int Type { get; set; }
        [Column("data")] public float Value { get; set; }

        public SensorType GetSensorType()
        {
            SensorType sensorType = 0;

            foreach (var type in Enum.GetValues<SensorType>())
                if ((int) type == Type)
                    sensorType = type;

            return sensorType;
        }
    }
}