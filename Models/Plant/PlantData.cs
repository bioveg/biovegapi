namespace BiovegAPI.Models.Plant
{
    public class PlantData
    {
        public PlantData(string name)
        {
            this.Name = name;
        }

        public string Name { get; }
    }
}