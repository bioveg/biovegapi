﻿namespace BiovegAPI.Models.Greenhouse
{
    public class GreenhouseData
    {
        public int Id { get; private set; }
        public string Name { get; private set; }

        public GreenhouseData(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
    }
}