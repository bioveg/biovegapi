﻿using System;
using System.Data;

namespace BiovegAPI.Models.ValueTime
{
    public class ValueTime
    {
        public DateTime DateTime { get; private set; }
        public float Value { get; private set; }

        public ValueTime(float value, DateTime dateTime)
        {
            this.Value = value;
            this.DateTime = dateTime;
        }
    }
}