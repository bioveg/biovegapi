﻿using System.Threading.Tasks;
using BiovegAPI.Hubs.BaseStation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace BiovegAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WaterController : ControllerBase
    {
        private readonly IHubContext<BaseStationHub> hubContext;

        public WaterController(IHubContext<BaseStationHub> hubContext) => this.hubContext = hubContext;

        [HttpPost]
        public async Task<IActionResult> Post([FromQuery] string data)
        {
            await this.hubContext.Clients.All.SendAsync("baseStation", data);
            return Ok("Watering plant bed...");
        }
    }
}