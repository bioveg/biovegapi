﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.Greenhouse;
using BiovegAPI.Repositories.Greenhouse;
using BiovegAPI.Services.Logger;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BiovegAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GreenhouseController : ControllerBase
    {
        private readonly IGreenhouseRepository greenhouseRepository;
        private readonly ILogger logger;

        public GreenhouseController(IGreenhouseRepository greenhouseRepository, ILogger logger)
        {
            this.greenhouseRepository = greenhouseRepository;
            this.logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> PostGreenhouse([FromQuery] GreenHouseDataWithEmail greenhouseData)
        {
            try
            {
                await this.greenhouseRepository.PostAsync(greenhouseData);
                return Ok(greenhouseData);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetGreenhouses([FromQuery] string email)
        {
            try
            {
                List<GreenhouseData> greenhouseDataList = await this.greenhouseRepository.GetAllAsync(email);
                string greenhouseDataListSerialized = JsonConvert.SerializeObject(greenhouseDataList);
                return Ok(greenhouseDataListSerialized);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}