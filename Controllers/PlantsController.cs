using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.Plant;
using BiovegAPI.Repositories.Plant;
using BiovegAPI.Services.Logger;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BiovegAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlantsController  : ControllerBase
    {
        private readonly IPlantRepository plantRepository;
        private readonly ILogger logger;

        public PlantsController(IPlantRepository plantRepository, ILogger logger)
        {
            this.plantRepository = plantRepository;
            this.logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetPlants()
        {
            try
            {
                List<PlantData> plantDataList = await this.plantRepository.GetAllAsync();
                string plantDataListSerialized = JsonConvert.SerializeObject(plantDataList);
                return Ok(plantDataListSerialized);
            }
            catch (Exception e)
            {
                logger.Log($"Plants Controller Exception: \n {e}");
                return Problem(e.Message);
            }
            
        }
    }
}