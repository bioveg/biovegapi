using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BiovegAPI.Models.Greenhouse;
using BiovegAPI.Models.Sensor;
using BiovegAPI.Repositories.Greenhouse;
using BiovegAPI.Repositories.Sensor;
using BiovegAPI.Services.Logger;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BiovegAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SensorController : ControllerBase
    {
        private readonly ISensorPostgresRepository sensorRepository;
        private readonly ILogger logger;

        public SensorController(ISensorPostgresRepository sensorPostgresRepository, ILogger logger)
        {
            this.sensorRepository = sensorPostgresRepository;
            this.logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> PostGreenhouse([FromQuery] string id, int greenhouseid, string plant)
        {
            try
            {
                await this.sensorRepository.PostAsync(id, greenhouseid, plant);
                return Ok();
            }
            catch (Exception e)
            {
                logger.Log($"SensorController: {e}");
                return Problem(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetGreenhouses([FromQuery] int greenhouseId)
        {
            try
            {
                List<Sensor> sensorList = await this.sensorRepository.GetSensors(greenhouseId);
                string sensorListSerialized = JsonConvert.SerializeObject(sensorList);
                return Ok(sensorListSerialized);
            }
            catch (Exception e)
            {
                return Problem("soemthing went wrong");
            }
        }
    }
}